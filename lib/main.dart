import 'package:flutter/material.dart';

enum APP_THEME{LIGHT,DARK}

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        iconTheme: IconThemeData(
          color: Colors.black54,
        ),
      ),
      dividerTheme: DividerThemeData(
          color: Colors.black54
      ),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: Color.fromRGBO(186, 255, 255,1),
          foregroundColor: Colors.black54
      ),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.blueGrey,
      ),
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      dividerTheme: DividerThemeData(
          color: Colors.white
      ),

    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}


class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppBar(),
        body: buildListView(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.light_mode),
          onPressed: () {
            setState(() {
              currentTheme == APP_THEME.DARK
                  ? currentTheme = APP_THEME.LIGHT
                  : currentTheme = APP_THEME.DARK;
            });
          },
        ),
      ),
    );
  }

  Widget buildCallButton() {
    return Column(
      children: <Widget>[
        Text("12:00 PM",style: TextStyle(fontSize: 15)),
        Image.asset('assets/icons/02d.png',height: 80),
        Text("29°C" , style: TextStyle(fontSize: 15)),
      ],
    );
  }

  Widget buildTextButton() {
    return Column(
      children: <Widget>[
        Text("13:00 PM",style: TextStyle(fontSize: 15)),
        Image.asset('assets/icons/02d.png',height: 80),
        Text("31°C" , style: TextStyle(fontSize: 15)),
      ],
    );
  }

  Widget buildVideoButton() {
    return Column(
      children: <Widget>[
        Text("14:00 PM",style: TextStyle(fontSize: 15)),
        Image.asset('assets/icons/09d.png',height: 80),
        Text("25°C" , style: TextStyle(fontSize: 15)),
      ],
    );
  }

  Widget buildEmailButton() {
    return Column(
      children: <Widget>[
        Text("15:00 PM",style: TextStyle(fontSize: 15)),
        Image.asset('assets/icons/01d.png',height: 80),
        Text("36°C" , style: TextStyle(fontSize: 15)),
      ],
    );
  }


  Widget mobilePhoneLiistTile() {
    return ListTile(
      leading: Image.asset("assets/icons/02d.png"),
      title: Text("Tuesday"),
      subtitle: Text("26°C"),
      
    );
  }

  Widget otherPhoneLiistTile() {
    return ListTile(
      leading: Image.asset("assets/icons/50d.png"),
      title: Text("Tuesday"),
      subtitle: Text("26°C"),

    );
  }

  Widget emailLiistTile() {
    return ListTile(
      leading: Image.asset("assets/icons/09d.png"),
      title: Text("Tuesday"),
      subtitle: Text("26°C"),

    );
  }

  Widget directLiistTile() {
    return ListTile(
      leading: Image.asset("assets/icons/10d.png"),
      title: Text("Tuesday"),
      subtitle: Text("26°C"),

    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0,
      //backgroundColor: Color.fromRGBO(186, 255, 255, 50),
      leading: BackButton(),
      actions: <Widget>[
        IconButton(onPressed: () {}, icon: Icon(Icons.location_on)),
      ],
      //iconTheme: IconThemeData(
      //color: Colors.black54,
      //),
    );
  }

  Widget buildListView() {
    return ListView(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              height: 150,
              child: Image.asset(
                'assets/icons/02d.png',
              ),
            ),
            Text("24°C",style: TextStyle(fontSize: 50 )),
            Text("Chonburi , Mostly Cloundy",style: TextStyle(color: Colors.grey , fontSize: 20)),

            Container(
              height: 50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        top: 15,
                        left: 15
                    ),
                    child: Text(
                      "Today",
                      style: TextStyle(
                          fontSize: 22,
                          fontFamily: 'Cheri',
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ],
              ),
            ),
            Divider(
              //color: Colors.black54,
            ),
            Container(
                margin: const EdgeInsets.only(top: 2, bottom: 5),
                child: dailyToday(),
                ),
            Divider(
              //color: Colors.black54,
            ),
            Text("Forecast for 7 days" , style: TextStyle(fontSize: 20),),
            Divider(
              //color: Colors.black54,
            ),

            mobilePhoneLiistTile(),
            otherPhoneLiistTile(),
            emailLiistTile(),
            directLiistTile(),
          ],
        ),
      ],
    );
  }

  Widget dailyToday() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        buildCallButton(),
        buildTextButton(),
        buildVideoButton(),
        buildEmailButton(),
      ],
    );
  }
}
